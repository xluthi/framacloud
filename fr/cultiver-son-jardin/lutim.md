# Installation de Lutim

![](images/lutim/lutim.jpg)

[Lutim][1] est le logiciel d’hébergement d’image que nous proposons sur [Framapic][2].

![](images/lutim/framapic.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite
  de ce tutoriel, nous supposerons que vous avez déjà fait pointer votre
  nom de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
  et que vous disposez d’un serveur dédié sous Debian.
</p>

## Prérequis

Lutim est codé en Perl, pour le faire fonctionner il est nécessaire
d’installer Carton, un gestionnaire de modules Perl.

    cpan Carton

Pour créer les aperçus des images après upload, il est nécessaire
d’installer le logiciel ImageMagick et le module Perl qui correspond.

    apt-get install perlmagick

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d’abord, connectez-vous en tant que `root` sur votre serveur et
créez un compte utilisateur `lutim` ainsi que le dossier `/var/www/lutim`
dans lequel seront copiés les fichiers avec les droits d’accès correspondants.

    useradd lutim
    groupadd lutim
    mkdir /var/www/lutim
    chown -R lutim:lutim /var/www/lutim

### 2 - Semer

![](images/icons/semer.png)

Téléchargez les fichiers de la dernière version sur le [dépôt officiel][3]
(« Download zip » en bas à droite ou bien en ligne de commande avec `git`),
copiez son contenu dans le dossier `/var/www/lutim` et attribuez
les droits des fichiers à l’utilisateur `lutim`

    cd /var/www/
    git clone https://framagit.org/luc/lutim.git
    chown lutim:lutim -R /var/www/lutim

![](images/lutim/lutim-fichiers.jpg)

Connectez-vous avec l’utilisateur `lutim` : `su lutim -s /bin/bash`
et lancez la commande d’installation des dépendances depuis le dossier `/var/www/lutim`

    cd /var/www/lutim
    su lutim -s /bin/bash
    carton install

Maintenant que tout est prêt, modifiez le fichier de configuration de
Lutim `lutim.conf` avec votre éditeur de texte préféré sur le modèle du
fichier `lutim.conf.template`.

Par défaut le logiciel est configuré pour écouter sur le port 8080 de
l'adresse 127.0.0.1 (localhost) et avec l’utilisateur `www-data` qu’il
faut donc ici remplacer par `lutim`

    cp lutim.conf.template lutim.conf
    vim lutim.conf

L’ensemble des paramètres sont facultatifs à l'exception du paramètre
`contact` (pensez bien à le configurer et à le décommenter) mais il peut
être utile de chiffrer systématiquement les images : `always_encrypt    => 1`

#### Lutim en tant que service

À présent, le serveur tournera lorsque qu’on lancera en tant que `root` cette commande :

    carton exec hypnotoad script/lutim

Pour éviter de devoir relancer le serveur à la main à chaque redémarrage
du serveur, on va donc lancer Lutim sous forme de service.
Il faut pour ça copier le script `utilities/lutim.init` dans le fichier
`/etc/init.d/lutim`, le rendre exécutable puis copier le fichier
`utilities/lutim.default` dans `/etc/default/lutim`.

    cp utilities/lutim.init /etc/init.d/lutim
    cp utilities/lutim.default /etc/default/lutim

Il faut maintenant modifier `/etc/default/lutim` pour y mettre le chemin
d’installation de notre Lutim
(`/var/www/lutim` si vous n’avez pas changé le chemin préconisé par ce tutoriel)

    vim /etc/default/lutim
    chmod +x /etc/init.d/lutim
    chown root:root /etc/init.d/lutim /etc/default/lutim

### 4 - Pailler

![](images/icons/pailler.png)

À ce stade, si tout s’est bien passé, lorsque vous exécutez la commande
`service lutim start`, Lutim est pleinement fonctionnel.
Vous n’avez qu’à vous rendre sur l’<abbr>URL</abbr> `http://127.0.0.1:8080` pour pouvoir l’utiliser.

![](images/lutim/lutim-web.jpg)

Nous allons maintenant configurer Lutim pour le rendre accessible depuis
un nom de domaine avec Nginx (vous pouvez également utiliser Apache ou
Varnish puisque seule la fonctionnalité de proxy inverse nous intéresse).

#### Nginx

Installez le paquet :

    apt-get install nginx

Créez le fichier de configuration de votre domaine
`/etc/nginx/sites-available/votre-nom-de-domaine` pour y mettre ceci
(en remplaçant « votre-nom-de-domaine ») et le port 8080 si vous
l’avez changé dans la configuration de Lutim :

    server {
        listen 80;

        server_name votre-nom-de-domaine;
        root /var/www/lutim/public;

        # Important pour la confidentialité des utilisateurs
        access_log off;
        error_log /var/log/nginx/lutim.error.log;

        # Paramètre important ! À adapter en fonction de la configuration de Lutim
        client_max_body_size 40M;

        location ~* ^/(img|css|font|js)/ {
            try_files $uri @lutim;
            add_header Expires "Thu, 31 Dec 2037 23:55:55 GMT";
            add_header Cache-Control "public, max-age=315360000";
        }

        location / {
            try_files $uri @lutim;
        }

        location @lutim {
            proxy_pass  http://127.0.0.1:8080;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Remote-Port $remote_port;
            proxy_redirect     off;
        }
    }

Activez votre fichier :

    ln -s /etc/nginx/sites-available/votre-nom-de-domaine /etc/nginx/sites-enabled/votre-nom-de-domaine

Enfin, relancez nginx : `service restart nginx`

### 5 - Tailler et désherber

![](images/icons/tailler.png)

La personnalisation de votre instance de Lutim passe par l’édition à la
main des fichiers css, images et javascript qui se trouvent dans le
dossier `public` et des fichiers du dossier `templates` ou `public`
pour les fichiers statiques.

Les fichiers de langues se trouvent dans le dossier `lib/Lutim/I18N`.

Pour pouvoir personnaliser et observer vos modifications en direct,
il vous faudra stopper temporairement le service `service lutim stop`
et le démarrer avec la commande :

    carton exec morbo script/lutim

Le serveur écoutera alors sur http://127.0.0.1:3000 mais vous pouvez
le faire écouter sur le même port qu’avec `hypnotoad` pour continuer à
passer par Nginx pendant votre développement :

    carton exec morbo script/lutim --listen=http://127.0.0.1:8080

![](images/lutim/tree-lutim.jpg)

 [1]: https://lut.im/
 [2]: https://framapic.org
 [3]: https://framagit.org/luc/lutim